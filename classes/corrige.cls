% Classe définie pour compilation avec XeTeX

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{corrige}[2020/06/15 v0.3 Classe pour les corrigés]

%%%%%%%%%%%%%%%%%%%
% Initialisation  %
%%%%%%%%%%%%%%%%%%%

% On passe les options non spécifiques à la classe de base
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{base}}
\ProcessOptions\relax

% Et on charge la classe de base
\LoadClass[corrige]{base}

% Puis viennent les spécificités de la classe corrigés

%%%%%%%%%%%%%%%%%%%%%%%%
%      Page style      %
%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{lastpage}
\def\BaseColor{LimeGreen}

\newpagestyle{corrige}{
	\sethead
	{
		\begin{tikzpicture}[remember picture,overlay]
		\fill[\BaseColor]
		(current page.north west) -- ([xshift=5cm]current page.north) -- ([xshift=3cm,yshift=75pt]current page.north|-current page text area.north) -- ([yshift=75pt]current page.north west|-current page text area.north west) -- cycle ;
		\node[font=\sffamily\bfseries\color{white},anchor=north west, xshift=55pt,yshift=-15pt] at (current page.north west) {\Huge \chaptertitle} ;
		\node[font=\sffamily\bfseries\color{BlackPen},anchor=north east,xshift=-1.5cm,yshift=-17pt] at (current page.north east) {\huge Cours} ;
		\end{tikzpicture}
	}{}{}
	\setfoot
	{
		\begin{tikzpicture}[remember picture,overlay]
		\fill[\BaseColor]
		(current page.south east) --
		([yshift=-30pt]current page.south east|-current page text area.south east) --
		([yshift=-30pt,xshift=7cm]current page.south|-current page text area.south) --
		([xshift=5cm]current page.south) --
		cycle ;
		\node[xshift=-2cm,yshift=.9cm,font=\bfseries\color{white}] at (current page.south east) {\large Page \thepage\ sur \pageref*{LastPage}};
		\end{tikzpicture}
	}{}{}
}
\pagestyle{corrige}

\RequirePackage{irthm}	% Environnements personnalisés dans un package perso

% Pour rétablir les paramètres par défaut (renommer 'reset' ?) -> voir avec les MAJ de tasks.sty
\NewDocumentCommand\labelexercise{}{
	\settasks{
		label-width = 1em,
		label-offset = .3333em,
		item-indent = 2.5em
	}
}

\NewDocumentCommand\bulletexercise{}{\settasks{label = $\bullet$}}

% Ligne suivante pour quand tasks.sty sera mis à jour dans TeXlive
%\NewTasksEnvironment[label={},label-width=0pt,label-offset=0pt,item-indent=0pt]{nolabelexercise}


\pagestyle{empty}
\AtBeginShipout{\AtBeginShipoutAddToBox{\Header\Footer}}
\AtBeginShipoutFirst{\Header\Footer}

\color{BlackPen}

% recommended to avoid some mailer issues: https://tex.stackexchange.com/questions/543465/need-advice-on-custom-class
\endinput