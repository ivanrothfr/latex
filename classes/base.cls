% Class to be compiled using XeTeX

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{base}[2020/06/15 v0.3 Classe de base pour les cours, exercices, corrigés et évaluations]

% Options
% dys : la police de texte devient OpenDyslexic, le corps plus grand et l'interligne est double
% gris : document en niveaux de gris
% print : liens désactivés et la police passe en serif

\makeatletter
	\def\basedate{\@date}
	\def\basetitle{\@title}
	\def\baseauthor{\@author}
\makeatother

% Fonts
\def\DocMainFont{Latin Modern Sans}	% Police pour le texte
\def\DocMathFont{XITS Math}			% Police pour les maths
\def\FontSize{11pt}					% Corps (peut être modifié par l'option 'dys' plus bas)
\def\LineSpacing{\onehalfspacing}	% Interligne (peut être modifié par l'option 'dys' plus bas)

%%%%%%%%%%%%%%%%%%%%%%%
% Gestion des options %
%%%%%%%%%%%%%%%%%%%%%%%

% Classe de base
\def\BaseClass{article}
\DeclareOption{corrige}{\def\BaseClass{article}}
\DeclareOption{cours}{\def\BaseClass{report}}

% À créer
%\DeclareOption{eval}{\def\BaseClass{exam}}
%\DeclareOption{exercices}{\def\BaseClass{article}}
%\DeclareOption{fiche}{\def\BaseClass{article}}
%\DeclareOption{livre}{\def\BaseClass{book}}

% Si l'option 'dys' est passée à la classe, la police par défaut devient OpenDyslexic
\DeclareOption{dys}{%
	\def\DocMainFont{OpenDyslexic}
	\def\DocMathFont{XITS Math}	% En attendant de trouver une police adaptée...
	\DeclareOption{corrige}{%	% Pour test. À intégrer plus proprement si ça fonctionne
		\def\BaseClass{scrartcl}
	}
	\def\FontSize{fontsize=13pt}
	\def\LineSpacing{\doublespacing}
}

% Si l'option 'print' :
% 	- polices avec empattements sauf si dys est passé en option
%	- liens désactivés
\DeclareOption{print}{%
	%\def\dysfalse{\let\def\DocMainFont{Linux Libertine G}\relax}
	\makeatletter
		\@ifclasswith{base}{dys}{}{\def\DocMainFont{Linux Libertine G}}	
	\makeatother
	%\def\DocMainFont{Linux Libertine G}
	\PassOptionsToPackage{draft}{hyperref}
}

% Si l'option 'gris' est passée à la classe, on active l'option gray de xcolor
\DeclareOption{gris}{\PassOptionsToPackage{gray}{xcolor}}

\ProcessOptions\relax


%%%%%%%%%%%%%%%%%%%
% Initialisation  %
%%%%%%%%%%%%%%%%%%%

% Classe article et taille de police par défaut
\LoadClass[a4paper,\FontSize]{\BaseClass}

% geometry
\RequirePackage[hmargin=2cm,bmargin=3cm,tmargin=4.5cm,centering]{geometry}

% Polices pour le texte (polices pour les  dans la section « Maths »)
\RequirePackage{fontspec}
\setmainfont{\DocMainFont}

% Localisation
\RequirePackage{polyglossia}
\setdefaultlanguage{french}

% Liens
\RequirePackage[colorlinks=true,urlcolor=BluePen,linkcolor=BluePen]{hyperref}

%%%%%%%%%%%%%
%  Patches  %
%%%%%%%%%%%%%

%%% Début
%%% Racine carrée et nombre à virgule

% La racine carrée ne s'affiche pas bien lorsqu'il y a des nombres décimaux sous le radical
% Un patch est proposé par egreg ici : 
% https://tex.stackexchange.com/questions/545915/decimal-number-and-sqrt-radical-height
\RequirePackage{letltxmacro}
\LetLtxMacro\ORIsqrt\sqrt

\DeclareRobustCommand{\sqrt}[2][]{%
	\begingroup
	\begingroup\lccode`~=`,\lowercase{\endgroup\let~}\smashedcomma
	\ifnum\mathcode`,="8000 \else\mathchardef\ORIcomma=\mathcode`, \fi
	\mathcode`,="8000
	\if\relax\detokenize{#1}\relax\ORIsqrt{#2}\else\ORIsqrt[#1]{#2}\fi
	\endgroup
}
\NewDocumentCommand\smashedcomma{}{\smash{\ORIcomma}}

%%% Racine carrée et nombre à virgule
%%% Fin


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Graphismes, couleurs et ornements  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Tikz charge aussi graphicx et xcolor
% On charge donc xcolor avant pour mettre d'autres options
%\RequirePackage[dvips,xetex]{graphicx}
\RequirePackage[usenames,dvipsnames]{xcolor}
\RequirePackage{tikz}
\RequirePackage{tikzpagenodes}
\usetikzlibrary{arrows,3d}

% Chemin par défaut pour les images
\graphicspath{{./img/}}

% Jeu de couleurs personnalisées
\definecolor{BluePen}{RGB}{46,116,181}
\definecolor{RedPen}{RGB}{240, 50, 50}
\definecolor{GreenPen}{RGB}{20, 140, 20}
\definecolor{BlackPen}{RGB}{40, 40, 40}
\definecolor{OrangePen}{RGB}{230, 150, 10}
\definecolor{LightGreen}{RGB}{25,170,75}

% Pour les ornements (à charger après xcolor)
\RequirePackage{pgfornament}


%%%%%%%%%
% Maths %
%%%%%%%%%

\RequirePackage{mathtools}

% Pour les maths en Unicode (remplace amsfonts, amssymb & co)
\RequirePackage[math-style=french]{unicode-math}

% Polices
\setmathfont{\DocMathFont}
\setmathfont[range={\mathcal,\mathbfcal},StylisticSet=1]{XITS Math}	% jeu spécial pour mathcal

% Police sans empattements


% Scratch 3
\RequirePackage{scratch3}

% Gestion des unités (voir plus bas « Commandes personnalisées »)
\RequirePackage[locale=FR]{siunitx}


%%%%%%%%%%%%%%%%%%%%
%      Divers      %
%%%%%%%%%%%%%%%%%%%%

%----------------------------
% Personnalisation des listes
%----------------------------

% Liste ordonnées personnalisées
\RequirePackage[shortlabels]{enumitem}

% Pour des numérotation multicolonnes de gauche à droite
\RequirePackage{tasks}

% Indentation des listes
\setlist{itemsep=.4em}
\setlist[1]{labelindent=\parindent}
\setlist[itemize]{label=$\bullet$}
\setlist[enumerate]{leftmargin=2.5pc}
\setlist[itemize,1]{leftmargin=1.5pc,topsep=.3em,itemsep=.2em}

%-------
% Divers
%-------

% Pour la mise en page sur plusieurs colonnes
\RequirePackage{multicol}

% Pour de jolis tableaux
\RequirePackage{booktabs}

% Espacement des lignes
\RequirePackage{setspace}
\LineSpacing

% Pas d'indentation pour les nouveaux paragraphes
\setlength\parindent{0pt}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Commandes personnalisées  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{xparse}

\renewcommand{\thefootnote}{\numeric{footnote}}

\NewDocumentCommand\prl{}{\mathbin{\!/\mkern-3mu/\!}}		% droites parallèles

% Unités \u...
% Le premier paramètre peut être passé directement à siunitx par exemple :
% [parse-numbers = false] pour utiliser des racines
% Le second est la valeur.
\NewDocumentCommand\umm{ o m }{\SI[#1]{#2}{\mm}}						% mm
\NewDocumentCommand\ucm{ o m }{\SI[#1]{#2}{\cm}}						% cm
\NewDocumentCommand\um{ o m }{\SI[#1]{#2}{\m}}							% m
\NewDocumentCommand\ug{ o m }{\SI[#1]{#2}{\g}}							% g
\NewDocumentCommand\ukg{ o m }{\SI[#1]{#2}{\kg}}						% kg
\NewDocumentCommand\usmm{ o m }{\SI[#1]{#2}{\square\mm}}				% mm²
\NewDocumentCommand\uscm{ o m }{\SI[#1]{#2}{\square\cm}}				% cm²
\NewDocumentCommand\usm{ o m }{\SI[#1]{#2}{\square\m}}					% m²
\NewDocumentCommand\uskm{ o m }{\SI[#1]{#2}{\square\km}}				% km²

\NewDocumentCommand\aire{ m }{{\ensuremath{\mathscr{A}}_{#1}}}			% Aire
\NewDocumentCommand\airet{ m }{\ensuremath{\mathscr{A}_{\text{#1}}}}	% Aire (indice texte)
\NewDocumentCommand\peri{ m }{\ensuremath{\mathscr{P}_{#1}}}			% Périmètre
\NewDocumentCommand\perit{ m }{\ensuremath{\mathscr{P}_{\text{#1}}}}	% Périmètre (indice texte)

% Théorème de Thalès
\NewDocumentCommand{\dethales}{ m m m m m m }{\ensuremath{\frac{#1}{#2} = \frac{#3}{#4} = \frac{#5}{#6}}}	% double égalité
\NewDocumentCommand{\sethales}{ m m m m }{\ensuremath{\frac{#1}{#2} = \frac{#3}{#4}}}						% simple égalité

% Devises
\NewDocumentCommand{\EUR}{}{\ensuremath{\,\text{\small EUR}}}			% EUR
\NewDocumentCommand{\EURH}{}{\ensuremath{\,\text{\small EUR HT}}}		% EUR HT
\NewDocumentCommand{\EURT}{}{\ensuremath{\,\text{\small EUR TTC}}}		% EUR TTC

% Pourcentages
\NewDocumentCommand{\pc}{}{\ensuremath{\,\%}}

% recommended to avoid some mailer issues: https://tex.stackexchange.com/questions/543465/need-advice-on-custom-class
\endinput