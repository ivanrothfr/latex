% Classe définie pour compilation avec XeTeX

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{cours}[2020/06/15 v0.3 Classe pour les cours]

%%%%%%%%%%%%%%%%%%%
% Initialisation  %
%%%%%%%%%%%%%%%%%%%

% On passe les options non spécifiques à la classe de base
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{base}}
\ProcessOptions\relax

% Et on charge la classe de base
\LoadClass[cours]{base}

% Puis viennent les spécificités de la classe cours

%%%%%%%%%%%%%%%
%  Titraille  %
%%%%%%%%%%%%%%%

\RequirePackage[pagestyles]{titlesec}

% Avec « Chapitre xx »
%\titleformat{\chapter}[display]{\normalfont\Large\bfseries\color{BrickRed}}{\chaptertitlename\ \thechapter}{20pt}{\Huge}
% Sans « Chapitre xx »
\titleformat{\chapter}[display]{\normalfont\Large\bfseries\color{BrickRed}}{}{20pt}{\Huge}
\titleformat*{\section}{\normalfont\Large\bfseries\color{BrickRed}}
\titleformat*{\subsection}{\normalfont\large\bfseries\color{GreenPen}}
\titleformat*{\subsubsection}{\normalfont\normalsize\bfseries\color{BluePen}}

% Sections
\renewcommand\thesection{\Roman{section}}	% Les sections seront au format I, II, ...
\renewcommand\thesubsection{\arabic{subsection}} % Les sous-sections : 1., 2., ...


%%%%%%%%%%%%%%%%%%%%%%%%
%      Page style      %
%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{lastpage}
\def\BaseColor{BrickRed}

% On définit le pagestyle plain comme étant empty car la classe report force le chargement de plain
\makeatletter
	\let\ps@plain\ps@empty
\makeatother

\newpagestyle{cours}{
	\sethead
	{
		\begin{tikzpicture}[remember picture,overlay]
		\fill[\BaseColor]
		(current page.north west) -- ([xshift=5cm]current page.north) -- ([xshift=3cm,yshift=75pt]current page.north|-current page text area.north) -- ([yshift=75pt]current page.north west|-current page text area.north west) -- cycle ;
		\node[font=\sffamily\bfseries\color{white},anchor=north west, xshift=55pt,yshift=-15pt] at (current page.north west) {\Huge \chaptertitle} ;
		\node[font=\sffamily\bfseries\color{BlackPen},anchor=north east,xshift=-1.5cm,yshift=-17pt] at (current page.north east) {\huge Cours} ;
		\end{tikzpicture}
	}{}{}
	\setfoot
	{
		\begin{tikzpicture}[remember picture,overlay]
		\fill[\BaseColor]
		(current page.south east) --
		([yshift=-30pt]current page.south east|-current page text area.south east) --
		([yshift=-30pt,xshift=7cm]current page.south|-current page text area.south) --
		([xshift=5cm]current page.south) --
		cycle ;
		\node[xshift=-2cm,yshift=.9cm,font=\bfseries\color{white}] at (current page.south east) {\large Page \thepage\ sur \pageref*{LastPage}};
		\end{tikzpicture}
	}{}{}
}
\pagestyle{cours}

\RequirePackage{irthm}	% Environnements personnalisés dans un package perso

\color{BlackPen}

% recommended to avoid some mailer issues: https://tex.stackexchange.com/questions/543465/need-advice-on-custom-class
\endinput